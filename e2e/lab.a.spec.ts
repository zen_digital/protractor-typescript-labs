import { browser } from 'protractor';

describe('TypeScript', () => {
  it('should get google', () => {
    browser.waitForAngularEnabled(false);
    browser.get('http://www.google.com');
    expect(browser.getTitle()).toBe('Google');
  });
});
