// Because this file imports from  protractor, you'll need to have it as a
// project dependency. Please see the reference config: lib/config.ts for more
// information. https://github.com/angular/protractor/blob/master/lib/config.ts
//
// Why you might want to create your config with typescript:
// Editors like JetBrain's WebStorm and Microsoft Visual Studio Code will have
// auto-complete and description hints.
//
// To run this example, first transpile it to javascript with `npm run tsc`,
// then run `protractor conf.js`. WebStorm can be configured to do this
// automatically
import { Config } from 'protractor';

export let config: Config = {
  directConnect: true,
  framework: 'jasmine',
  capabilities: {
    browserName: 'chrome'
  },
  specs: ['./e2e/**/*.spec.js'],
  // seleniumAddress: 'http://localhost:4444/wd/hub',

  // You could set no globals to true to avoid jQuery '$' and protractor '$'
  // collisions on the global namespace.
  noGlobals: true
};
